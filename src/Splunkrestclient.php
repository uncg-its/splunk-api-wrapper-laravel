<?php

namespace Uncgits\Splunkrestclientlaravel;

use Illuminate\Support\Facades\Cache;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Support\Facades\Log;

use Uncgits\Splunkrestclient\Splunkrestclient as Baseapi;

class Splunkrestclient extends Baseapi
{
    protected $historyMonths;

    protected $cacheActive;
    protected $endpointsToCache;
    protected $cacheMinutes;

    protected $debugMode;

    public function __construct($token = '')
    {
        // set vars specific to this instance of the client for laravel
        $this->setApiHost(config('splunkrestclient.host'));

        $this->setPort(config('splunkrestclient.port'));

        $this->setToken($token === '' ? config('splunkrestclient.token') : $token);

        // token set during instatiation from DB value

        if (config('splunkrestclient.use_proxy') == 'true') {
            $this->setUseProxy(true);
            $this->setProxyHost(config('splunkrestclient.proxy_host'));
            $this->setProxyPort(config('splunkrestclient.proxy_port'));
        }
    }

    /**
     * apiCall() - performs a call to the Splunk API
     *
     * @param string $function - function / endpoint (per Splunk API docs)
     * @param string $method - HTTP request method (e.g. GET, POST, etc.)
     * @param array $params (optional) - Parameters (for GET), or body content (for POST et. al.)
     * @param boolean $download (optional) - whether the call should expect to get a file download. If false, the call expects JSON.
     *
     * @return array
     */

    protected function apiCall($endpoint, $method, $params = [], $download = false)
    {
        $isDownload = $download ? "download" : "no-download";

        if ($this->cacheActive) {
            $hashedCacheKey = hash('sha256', $endpoint . $method . json_encode($params) . $isDownload);
            $this->debugMessage('Hashed cache key: ' . $hashedCacheKey);
        }

        if ($this->cacheActive && Cache::has($hashedCacheKey)) {
            // if cache active AND hashed value exists
            $doApiCall = false;
        } else {
            // cache is inactive OR cached value does not exist
            $doApiCall = true;
        }

        if ($doApiCall) {
            // doing the live API call is needed

            $this->debugStartMeasure('apiCall', 'Making API call to Splunk');
            $resultArray = parent::apiCall($endpoint, $method, $params, $download);
            $this->debugStopMeasure('apiCall');

            $created = time();

            if ($resultArray['response']['httpCode'] == 200) {
                $messageLevel = 'success';
                $message = 'API call to ' . $endpoint . ' successful.';

                if ($this->cacheActive && $method == 'get') {
                    // cache if the call was successful AND it was a GET request AND we are supposed to cache it...
                    Cache::put($hashedCacheKey, $resultArray, $this->cacheMinutes);
                    Cache::put($hashedCacheKey . '-created', $created, $this->cacheMinutes);
                    $message .= ' Result cached in API cache.';
                } else {
                    $message .= ' Result not cached in API cache.';
                }
            } else {
                $message = 'API error calling ' . $endpoint . ': ' . $resultArray['response']['httpCode'] . ' - ' . $resultArray['response']['httpReason'] . ' - ' . json_encode($resultArray['body']);
                $messageLevel = 'danger';
            }



            $returnArray = [
                'source'  => 'api',
                'created' => $created
            ];
        } else {
            // doing the live API call is not needed, which means the cache is required
            $resultArray = Cache::get($hashedCacheKey);
            $returnArray = [
                'source'  => 'cache',
                'created' => Cache::get($hashedCacheKey . '-created', 'unknown')
            ];
            $message = 'API data for ' . $endpoint . ' retrieved from cache (created: ' . $returnArray['created'] . ')';
            $messageLevel = 'success';
        }

        // handle notifications / logging / etc.
        $notificationMode = explode(',', config('splunkrestclient.notification_mode'));

        if (in_array('flash', $notificationMode)) {
            flash($message, $messageLevel);
        }

        if (in_array('log', $notificationMode)) {
            if ($messageLevel == 'danger') {
                Log::error($message);
            } else {
                Log::info($message);
            }
        }

        $finalArray = array_merge($returnArray, $resultArray);

        $this->debugInfo($finalArray);

        return array_merge($finalArray);
    }

    // debug helpers

    protected function debugMessage($message, $label = null)
    {
        if ($this->debugMode) {
            if (!is_null($label)) {
                Debugbar::addMessage($message, $label);
            } else {
                Debugbar::addMessage($message);
            }
        }
    }

    protected function debugInfo($object)
    {
        if ($this->debugMode) {
            Debugbar::info($object);
        }
    }

    protected function debugStartMeasure($key, $label = '')
    {
        if ($this->debugMode) {
            Debugbar::startMeasure($key, $label);
        }
    }

    protected function debugStopMeasure($key)
    {
        if ($this->debugMode) {
            Debugbar::stopMeasure($key);
        }
    }
}
