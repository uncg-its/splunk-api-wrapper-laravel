<?php

namespace Uncgits\Splunkrestclientlaravel\Facades;

use Illuminate\Support\Facades\Facade;

class SplunkApi extends Facade
{

    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'SplunkApi'; // the IoC binding.
    }
}
