# Splunk Rest Client - Laravel Wrapper

Contact: its-laravel-devs-l@uncg.edu

# Introduction

This package is a Laravel wrapper for the UNCG Splunk Rest Client API PHP Library package, so that the Splunk API PHP Library can be used in Laravel apps.

> This is a **work in progress**. Not recommend for production apps just yet.

---

# Installation

1. `composer require 'uncgits/splunk-rest-client-laravel-wrapper'`
2. **Laravel <5.5 only** - Add `Uncgits\Splunkrestclientlaravel\ServiceProvider::class,` to your `config/app.php` file
3. Run `php artisan vendor:publish --provider='Uncgits\Splunkrestclientlaravel\ServiceProvider` - to publish the `splunkrestclient.php` config file
4. Set your environment credentials in your `.env` file, and set your configuration options in `config/splunkrestclient.php`

```
# SPLUNK HTTP Event Collector
SPLUNK_HTTP_COLLECTOR_HOST=https://
SPLUNK_HTTP_COLLECTOR_PORT=8088
SPLUNK_HTTP_COLLECTOR_SOURCE_TYPE=
SPLUNK_HTTP_COLLECTOR_TOKEN=
SPLUNK_HTTP_COLLECTOR_USE_PROXY=false
```

## Dependencies

This package has dependencies on `uncgits/splunk-rest-client`

---

# Usage

Example of usage:

```php

use Uncgits\Splunkrestclientlaravel\Splunkrestclient;

$this->splunkrestclient = new Splunkrestclient();

$dataArray = [
    'total_users'   => 20385,
    'total_storage' => 72445624687327,
];

$optionsArray = [
    'sourcetype'   => '{agreed-sourcetype-with-splunk-admin}',
    'event'         => json_encode($dataArray),
];

$data = $this->splunkrestclient->sendData($optionsArray);

```

## Basic Usage / Getting Started

In your code, assuming you've set your information/credentials properly in your `.env` file, you should be able to instantiate the `Uncgits\Splunk\Restclientwrapper\Splunkrestclient` class, and then use any of its available methods (inherited from `Uncgits\Splunkrestclient\Splunkrestclient`) to make an API call.

## Custom token

By default the `Splunkrestclient` class will reference the token you set in your `.env` file. But you can also pass it another token as an argument when instantiating the class. Example:

```php
$api = new Uncgits\Splunkrestclientlaravel\Splunkrestclient('my-token-here');
```

## Facade

You can also use the `SplunkApi` facade instead of instantiating the class:

```php
$result = SplunkApi::sendData([]);
```

---

# Version History

## 0.3.0

- Open Source BSD license
- bump REST client version to 0.2 (open source)

## 0.2.2

- require rest client version 0.1 (cosmetic, fixes PSR-4)

## 0.2.1

- change to PSR-4 declaration since we were already following it

## 0.2

- Fixes config keys for HTTP proxy

> NOTE: if upgrading, make sure you add the new config keys into your `splunkrestclient.php` config file, and set them in your `.env` file if using a proxy.

## 0.1

 - First versioned release
